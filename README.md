# 6du

## 初始化项目

运行以下命令，会克隆所有GIT仓库。

```
bash <(curl -sL https://gitlab.com/6du/6du/raw/master/init.sh)
```

扩展阅读 : [Git 工具 - 子模块](https://git-scm.com/book/zh/v1/Git-%E5%B7%A5%E5%85%B7-%E5%AD%90%E6%A8%A1%E5%9D%97)

## 目录结构

1. sh
   
   静态博客的命令工具, 运行 `npm link` 安装

1. site
   
   静态博客的源代码

1. 6du.gitlab.io
   
   官方演示站
